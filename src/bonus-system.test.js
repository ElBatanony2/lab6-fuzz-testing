import { calculateBonuses as calc } from './bonus-system';

describe('Bonus System Tests', () => {
    test('Standard Bonus 1', () => {
        expect(calc('Standard', 1)).toEqual(0.05);
    });
    test('Standard Bonus 1', () => {
        expect(calc('Standard', 9999)).toEqual(0.05);
    });
    test('Standard Bonus 2', () => {
        expect(calc('Standard', 10000)).toEqual(0.05 * 1.5);
    });
    test('Standard Bonus 3', () => {
        expect(calc('Standard', 50000)).toEqual(0.05 * 2);
    });
    test('Standard Bonus 4', () => {
        expect(calc('Standard', 100000)).toEqual(0.05 * 2.5);
    });

    test('Zero bonus', () => {
        expect(calc('Yo yo', 100000)).toEqual(0);
    });

    test('Negative bonus', () => {
        expect(calc('Premium', -10)).toEqual(0.1);
    });

    test('Diamond bonus', () => {
        expect(calc('Diamond', 50000)).toEqual(0.4);
    });
})